﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MyClient.Models;
using DemoServiceReference;
using System.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace MyClient.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        
        private IConfiguration _config;

        public MemberConfig _memberConfig { get; set; }


        private DemoServiceClient demoserClient = new DemoServiceClient();
        public HomeController(ILogger<HomeController> logger, IConfiguration config, IOptions<MemberConfig> memberConfig)
        {
            _logger = logger;
            _config = config;
            _memberConfig = memberConfig.Value;
        }

        public IActionResult Index()
        {
            ViewBag.result1 = demoserClient.HelloAsync().Result;
            ViewBag.result2 = demoserClient.HiAsync("ABC").Result;
            ViewBag.result3 = demoserClient.SumAsync(1,2).Result;
            ViewBag.result4 = _config.GetValue<string>("Member:Account");
            ViewBag.result5 = _config.GetValue<string>("Member:Password");
            ViewBag.result6 = _memberConfig.Account;
            ViewBag.result7 = _memberConfig.Password;

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
