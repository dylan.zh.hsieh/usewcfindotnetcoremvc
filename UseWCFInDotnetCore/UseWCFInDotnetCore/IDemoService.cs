﻿using System.ServiceModel;

namespace UseWCFInDotnetCore
{
    // 注意: 您可以使用 [重構] 功能表上的 [重新命名] 命令同時變更程式碼和組態檔中的介面名稱 "IDemoService"。
    [ServiceContract]
    public interface IDemoService
    {
        [OperationContract]
        string Hello();
        
        [OperationContract]
        string Hi(string name);

        [OperationContract]
        int Sum(int a, int b);
    }
}
