﻿namespace UseWCFInDotnetCore
{
    // 注意: 您可以使用 [重構] 功能表上的 [重新命名] 命令同時變更程式碼、svc 和組態檔中的類別名稱 "DemoService"。
    // 注意: 若要啟動 WCF 測試用戶端以便測試此服務，請在 [方案總管] 中選取 DemoService.svc 或 DemoService.svc.cs，然後開始偵錯。
    public class DemoService : IDemoService
    {
        public string Hello()
        {
            return "Hello World";
        }

        public string Hi(string name)
        {
            return "Hi" + name;
        }

        public int Sum(int a, int b)
        {
            return a + b;
        }
    }
}
