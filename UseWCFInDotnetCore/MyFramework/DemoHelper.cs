﻿using DemoServiceReference;
using System.Threading.Tasks;

namespace MyFramework
{
    public class DemoHelper
    {
        private DemoServiceClient demoServiceClient = new DemoServiceClient();

        public string HelloWorld()
        {
            Task<string> taskHello = demoServiceClient.HelloAsync();
            return taskHello.Result;
        }

        public string Hi(string name)
        {
            Task<string> taskHi = demoServiceClient.HiAsync(name);
            return taskHi.Result;
        }

        public int Sum(int a, int b)
        {
            Task<int> taskSum = demoServiceClient.SumAsync(a, b);
            return taskSum.Result;
        }
    }
}
