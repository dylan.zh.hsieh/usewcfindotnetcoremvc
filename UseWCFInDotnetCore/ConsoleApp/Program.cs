﻿using System;
using MyFramework;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            DemoHelper helper = new DemoHelper();
            Console.WriteLine(helper.HelloWorld());
            Console.WriteLine(helper.Hi("Dylan"));
            Console.WriteLine(helper.Sum( 99, 1));
        }
    }
}
